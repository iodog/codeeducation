## Resolução do Desafio 1 de Docker do Curso [Full Cycle 3.0](https://fullcycle.com.br/)
&nbsp;
&nbsp;
### URL da imagem no Docker Hub: <https://hub.docker.com/r/iodog/codeeducation>

```
sudo docker pull iodog/codeeducation
```
&nbsp;

### Para rodar a aplicação:

```
sudo docker run --rm iodog/codeeducation
```
&nbsp;
&nbsp;

---

### URL do repositório: <https://gitlab.com/iodog/codeeducation>

---
&nbsp;
&nbsp;

#### Para criar a imagem:

```
sudo docker build --no-cache -t="iodog/codeeducation" .
```
&nbsp;
#### Para testar a imagem criada:

```
sudo docker run --rm iodog/codeeducation
```
&nbsp;
#### Para enviar a imagem para o Docker Hub:

```
sudo docker push iodog/codeeducation
```