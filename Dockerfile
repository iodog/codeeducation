FROM golang:1.19.1 AS builder
WORKDIR /usr/src/app
COPY . .
RUN go build -v app.go

FROM scratch
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/app /usr/src/app/app
ENTRYPOINT [ "/usr/src/app/app" ]